# MBZIRC Truck detection node #

### What is this repository for? ###

This code estimates the position and orientation of the truck in the MBZIRC Challenge.

### How do I get set up? ###

To use the code on the on-board Manifold computer, please consult the [Launch Readme](https://bitbucket.org/castacks/mbzirc_launch).

To use the code in the simulation environment, please consult the [Simulation Readme](https://bitbucket.org/castacks/mbzirc_simulation).


### Who do I talk to? ###

* Guilherme Pereira (gpereira@ufmg.br)
* Azarakhsh Keipour (keipour@gmail.com)