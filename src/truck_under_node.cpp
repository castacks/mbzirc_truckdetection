#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_listener.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Int32.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#define IDLE			(1 << 0)
#define GOTOWATCHPOS            (1 << 1)
#define GOTOTRUCK	        (1 << 2)
#define TRACK		        (1 << 3)
#define WATCHPOS		(1 << 4)
#define GIMBALTRACK 		(1 << 5)
#define LANDING 		(1 << 6)
#define GIMBALDOWN		(1 << 7)



bool compareKeypoints(const cv::KeyPoint& i,const cv::KeyPoint& j) { return (i.size>j.size); }

class TruckDetector
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::CameraSubscriber image_sub_;
  ros::Publisher truck_pub;
  cv_bridge::CvImagePtr cv_ptr_;
  cv::Mat current_img_, previous_img_, error_img_;
  bool first_;  
  bool show_images_;
  int num_blobs_;
  int min_size_;

public:
  TruckDetector()
    : it_(nh_)
  {
    first_=true;
    image_sub_ = it_.subscribeCamera("/dji_sdk/image_raw", 1, &TruckDetector::imageCallback, this);
    truck_pub = nh_.advertise<std_msgs::Int32>("/truck_detection/truck_found", 50);
    ros::Duration(1.0).sleep();  
    // previous_image_time_=ros::Time::now();
    show_images_ = true;
    nh_.getParam("/truck_detection/show_images",show_images_);
    nh_.getParam("/truck_detection/num_blobs",num_blobs_);
    nh_.getParam("/truck_detection/min_size",min_size_);
  }

   

  void imageCallback(const sensor_msgs::ImageConstPtr& msg, const sensor_msgs::CameraInfoConstPtr& info_msg)
  {
      
    int mode_;
      nh_.getParamCached("/mission/mode", mode_); 
      if ((mode_ & WATCHPOS) == false) return;

    // ros::Duration time_between_images_ = msg->header.stamp - previous_image_time_;  
    
    // if (abs(time_between_images_.toSec()) > 0.3){ // This is to get more spaced images and increase the error between two images

    // previous_image_time_=msg->header.stamp;

    try
    {
      cv_ptr_ = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }      

    cv::cvtColor(cv_ptr_->image, current_img_, CV_BGR2GRAY);

    if(first_){
      first_ = false;
      previous_img_=current_img_.clone();
      return;
    }
    
    cv::absdiff(current_img_,previous_img_,error_img_);     

    // cv:threshold(error_img_, error_img_, 100, 255, 0);
    // cv::imshow("Raw Error Image", error_img_);
    // cv::waitKey(3);
    cv::erode(error_img_, error_img_,  cv::Mat(), cv::Point(-1, -1), 1);   // This can be used if there are other movement in scene
    // cv::dilate(error_img_, error_img_,  cv::Mat(), cv::Point(-1, -1), 4);

    if(show_images_){
      cv::imshow("Error Image", error_img_);
    }

    cv::SimpleBlobDetector::Params params;
  
    params.filterByArea = true;
    params.minArea = min_size_;
    params.maxArea = 5000.0;
    params.filterByColor= false;
    params.filterByCircularity = false;
    params.filterByConvexity = false;
    params.filterByInertia = false;
  
    cv::SimpleBlobDetector detector(params);
 
    // // Detect blobs.
    std::vector<cv::KeyPoint> keypoints;
    detector.detect(error_img_, keypoints);
    //ROS_INFO("Keypoints %d", (int)keypoints.size());

    if(show_images_){
      cv::drawKeypoints( cv_ptr_->image, keypoints, cv_ptr_->image, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    }

        
    if (keypoints.size()>num_blobs_){
      std_msgs::Int32 found_truck;
      found_truck.data = keypoints.size();
      truck_pub.publish(found_truck);

      // std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);  // sort the keypoints by the area
    // ROS_INFO("*************************");
    // for (int i=0; i<keypoints.size(); i++){
    //   ROS_INFO("Keypoints area %f", (float) keypoints[i].size);
    // } 
    }
//     else{
//       std_msgs::Int32 found_truck;
//       found_truck.data = 0;
//       truck_pub.publish(found_truck);
//     }

    previous_img_=current_img_.clone();

    if(show_images_){
      cv::imshow("Image Window", cv_ptr_->image);
      cv::waitKey(4);
    }
    // Update GUI Window
    
   } // if (time_between..
    
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "truck_detection");
  TruckDetector dt;
  ros::spin();
  return 0;
}
