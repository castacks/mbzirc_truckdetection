#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_listener.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "mbzirc_prediction/trackDefinition.h"

#define SHOWIMAGES

bool compareKeypoints(const cv::KeyPoint& i,const cv::KeyPoint& j) { return (i.size>j.size); }

class TruckDetector
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::CameraSubscriber image_sub_;
  ros::Publisher odom_pub_;
  cv_bridge::CvImagePtr cv_ptr_;
  cv::Mat current_img_, previous_img_, error_img_;
  bool first_;
  ros::Time previous_image_time_;
  image_geometry::PinholeCameraModel cam_model_;
  tf::TransformListener tf_listener_;
  ca::Track *track_;
  std::vector<cv::Point2f> track_points_world_;
  nav_msgs::Odometry prev_truck_pose_; 
      
public:
  TruckDetector()
    : it_(nh_)
  {
    
    first_=true;

    image_sub_ = it_.subscribeCamera("/camera/image_raw", 1, &TruckDetector::imageCb, this);
    odom_pub_ = nh_.advertise<nav_msgs::Odometry>("/truck_detection/odom", 50);
    
    ros::Duration(1.0).sleep();  

    tf::StampedTransform transform;
    if (tf_listener_.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(10.0)) ) {
       ROS_INFO("Truck Detection - Using available transformation for track");
       tf_listener_.lookupTransform("/world", "/track", ros::Time(0), transform);
       track_ = new ca::Track(transform.getOrigin().x(), transform.getOrigin().y(), transform.getRotation().getAngle());	
    }
    else{
       ROS_WARN("Truck Detection - No transformation received - Assuming track at (0,0)");
       track_ = new ca::Track(0, 0, 0);
    } 

    track_ = new ca::Track(transform.getOrigin().x(), transform.getOrigin().y(), transform.getRotation().getAngle());
    
    for (int i=0;i<track_->waypoints.size();i++){
    	cv::Point2f waypoint(track_->waypoints[i].pose.pose.position.x, track_->waypoints[i].pose.pose.position.y);
        track_points_world_.push_back(waypoint);
    }

    previous_image_time_=ros::Time::now();

  }

   

  void imageCb(const sensor_msgs::ImageConstPtr& msg, const sensor_msgs::CameraInfoConstPtr& info_msg)
  {
    
    ros::Duration time_between_images_ = msg->header.stamp - previous_image_time_;  
    
    if (abs(time_between_images_.toSec()) > 0.3){ // This is to get more spaced images and increase the error between two images

      previous_image_time_=msg->header.stamp;

      try
      {
        cv_ptr_ = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      }
      catch (cv_bridge::Exception& e)
      {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
      }      
 
      cv::cvtColor(cv_ptr_->image, current_img_, CV_BGR2GRAY);

      if  (!first_){
	error_img_= current_img_ - previous_img_;     

        cv:threshold(error_img_, error_img_, 100, 255, 0);
        // cv::imshow("Raw Error Image", error_img_);
        // cv::waitKey(3);
        // cv::erode(error_img_, error_img_,  cv::Mat(), cv::Point(-1, -1), 1);   // This can be used if there are other movement in scene
        cv::dilate(error_img_, error_img_,  cv::Mat(), cv::Point(-1, -1), 4);

#ifdef SHOWIMAGES 
        cv::imshow("Error Image", error_img_);
#endif 
        
        cv::SimpleBlobDetector::Params params;
      
        params.filterByArea = true;
        params.minArea = 50.0;
        params.maxArea = 5000.0;
        params.filterByColor= false;
        params.filterByCircularity = false;
	params.filterByConvexity = false;
	params.filterByInertia = false;
	
	cv::SimpleBlobDetector detector(params);
 
	// Detect blobs.
	std::vector<cv::KeyPoint> keypoints;
	detector.detect(error_img_, keypoints);
        //ROS_INFO("Keypoints %d", (int)keypoints.size());

#ifdef SHOWIMAGES
        cv::drawKeypoints( cv_ptr_->image, keypoints, cv_ptr_->image, cv::Scalar(0,0,255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
#endif
        
        if (keypoints.size()>1){
         std::sort(keypoints.begin(), keypoints.end(), compareKeypoints);  // sort the keypoints by the area
        // ROS_INFO("*************************");
        // for (int i=0; i<keypoints.size(); i++){
        //   ROS_INFO("Keypoints area %f", (float) keypoints[i].size);
        // } 
        } 


        // Get the most recent transform between the camera frame and the world
        tf::StampedTransform transform;
	try{
	  tf_listener_.lookupTransform(cam_model_.tfFrame(), "/world", ros::Time(0), transform);
          std::vector<cv::Point2f> track_points_image; 
          // Project the points of the track into the image
          for (int i=0;i<track_->waypoints.size();i++){
      	
            // Create point to be transformed to the image 
            geometry_msgs::PointStamped pt_track_world, pt_track_camera;
            pt_track_world.header.frame_id = track_->waypoints[i].header.frame_id; 
            pt_track_world.header.stamp = ros::Time();
            pt_track_world.point.x = track_->waypoints[i].pose.pose.position.x;
            pt_track_world.point.y = track_->waypoints[i].pose.pose.position.y;
            pt_track_world.point.z = track_->waypoints[i].pose.pose.position.z+0.75; // Aproximate height of the truck
  
            // Transform to the camera reference frame 
            tf_listener_.transformPoint(cam_model_.tfFrame(), pt_track_world, pt_track_camera);
        
            // Transform to the image plane 
            cv::Point3d pt_track_cam(pt_track_camera.point.x, pt_track_camera.point.y, pt_track_camera.point.z);
            cv::Point2d uv;
            uv = cam_model_.project3dToPixel(pt_track_cam);
            track_points_image.push_back(uv);
#ifdef SHOWIMAGES        
      	    cv::circle(cv_ptr_->image, uv, 5, CV_RGB(0,0,255), -1);
#endif
          } // for
        
          if (keypoints.size()>0){  // If found keypoints
	   
            // Project the points of the truck found in the image into the track   
            cv::Mat H = cv::findHomography(track_points_image, track_points_world_);
            std::vector<cv::Point2f> truck_uv, truck_world;
            for (int i=0;i<keypoints.size();i++)  
              	truck_uv.push_back(keypoints[i].pt);
            cv::perspectiveTransform(truck_uv, truck_world, H); // Transform the keypoints on the image to the world

	    // Decides if one of the keypoints is the truck and publishes odometry information
            for (int i=0;i<truck_world.size();i++) {
               	
           	nav_msgs::Odometry truck_pose;
           	truck_pose.header.frame_id = track_->waypoints[0].header.frame_id;
           	truck_pose.header.stamp = ros::Time::now();
           	truck_pose.pose.pose.position.x=truck_world[i].x;
           	truck_pose.pose.pose.position.y=truck_world[i].y;
           	truck_pose.pose.pose.position.z=0.0;  
           	if  (track_->inTrack(truck_pose, 3)){    // If the position is 3m or less from the center of the track...
                        double dx=truck_pose.pose.pose.position.x-prev_truck_pose_.pose.pose.position.x;
                        double dy=truck_pose.pose.pose.position.y-prev_truck_pose_.pose.pose.position.y;
                        tf::Matrix3x3 m;
			tf::Quaternion q;
			m.setRPY(0,0,atan2(dy,dx));      // ... computes the orientation...  
                        m.getRotation(q);
 			truck_pose.pose.pose.orientation.x=(double)q.x();
			truck_pose.pose.pose.orientation.y=(double)q.y();
			truck_pose.pose.pose.orientation.z=(double)q.z();
			truck_pose.pose.pose.orientation.w=(double)q.w();
                	odom_pub_.publish(truck_pose);   // ... publishes odometry information...
                        prev_truck_pose_=truck_pose;     // ... stores the information and...
                   	break;                           // ... leaves the loop. The blob with largest area on the track is the truck. 
           	}
            } // for  
          } // if keypoints.size  
        } // try
	catch (tf::TransformException ex){ // Cannot get transformations
            ROS_WARN("CANNOT COMPUTE POSE! %s",ex.what());           
        } // catch
        
      } // if !first
      else { 
          cam_model_.fromCameraInfo(info_msg); // Get the camera parameters in the first image
      } 
           
      previous_img_=current_img_.clone();

#ifdef SHOWIMAGES        
      // Update GUI Window
      cv::imshow("Image Window", cv_ptr_->image);
      cv::waitKey(3);
#endif

      first_=false;
   } // if (time_between..
    
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "truck_detection");
  TruckDetector dt;
  ros::spin();
  return 0;
}
